var express = require('express')
const bodyParser= require('body-parser')
const app = express()
const MongoClient = require('mongodb').MongoClient
var nodeThen = require('node-then');

app.set('view engine', 'ejs')

app.use('/music', express.static('music'))
app.use('/fifty', express.static('fifty'))
app.use('/country', express.static('country'))
app.use('/drake', express.static('drake'))
app.use('/weekend', express.static('weekend'))
app.use('/ruby', express.static('ruby'))
app.use('/views', express.static('views'))
app.use('/shawn', express.static('shawn'))
app.use('/b', express.static('b'))

app.use(bodyParser.urlencoded({extended: true}))

const fs = require('fs');

var db
var musicSrc = 'Loote-High-Without-Your-Love-yoodownload.com.mp3';

app.get('/', (req, res) => {
  res.redirect('/musicMUSIC/MUSIC');
})
app.get('/music', (req, res) => {
  res.redirect('/musicMUSIC/MUSIC');
})
app.get('/fifty', (req, res) => {
  res.redirect('/fiftyMUSIC/MUSIC');
})
app.get('/country', (req, res) => {
  res.redirect('/countryMUSIC/MUSIC');
})
app.get('/drake', (req, res) => {
  res.redirect('/drakeMUSIC/MUSIC');
})
app.get('/ruby', (req, res) => {
  res.redirect('/rubyMUSIC/MUSIC');
  var output = getSearchArray();
})
app.get('/weekend', (req, res) => {
  res.redirect('/weekendMUSIC/MUSIC');
})
app.get('/shawn', (req, res) => {
  res.redirect('/shawnMUSIC/MUSIC');
})
app.get('/b', (req, res) => {
  res.redirect('/bMUSIC/MUSIC');
})
app.get('/musicMUSIC/:search', (req, res) => {
  fs.readdir('./music/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'music', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/fiftyMUSIC/:search', (req, res) => {
  fs.readdir('./fifty/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'fifty', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/countryMUSIC/:search', (req, res) => {
  fs.readdir('./country/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'country', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/drakeMUSIC/:search', (req, res) => {
  fs.readdir('./drake/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'drake', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/rubyMUSIC/:search', (req, res) => {
  fs.readdir('./ruby/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'ruby', musicSearchList:output, searchTerm: req.params.search});
    });
  })
})
app.get('/weekendMUSIC/:search', (req, res) => {
  fs.readdir('./weekend/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'weekend', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/shawnMUSIC/:search', (req, res) => {
  fs.readdir('./shawn/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'shawn', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.get('/bMUSIC/:search', (req, res) => {
  fs.readdir('./b/', (err, files) => {
    getSearchArray( function (output) {
      res.render('index.ejs', { musicSrc: files, get:'b', musicSearchList:output, searchTerm: req.params.search});
    });  })
})
app.listen(process.env.PORT || 3000, () => {
  console.log('listening on 3000')
})

function getSearchArray(callback){
  var songList = [];
  fs.readdir('./fifty/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./b/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'b/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./shawn/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'shawn/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./weekend/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'weekend/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./music/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./drake/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./fifty/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./country/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
  })
  fs.readdir('./ruby/', (err, files) => {
    for (var i = 0; i < files.length; i++) {
      files[i] = 'fifty/'+files[i];
    }
    songList = songList.concat(files);
    if (callback) {
      callback(songList);
    }
    //callback(songList);
  })
}
function returnArray() {
  var arrayToReturn = [
    'hello',
    'goodbye',
    'maybe'
  ]
  return arrayToReturn
}
