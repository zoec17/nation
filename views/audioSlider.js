

(function(){
            //simple feature test
            if(!document.createElement('video').canPlayType){
                return;
            }


            //the mediaplayer is the wrapper for all controls
            function createPlayer(mediaplayer){
                var video = document.getElementById('audio');
                var seekBar = mediaplayer.querySelector('.seekbar');

                if(seekBar){
                    createSeekBar(video, seekBar);
                }
            }

            function createSeekBar(video, seekBar){
                var duration, videoWasPaused;
                var blockSeek = false;

                function enableDisableSeekBar(){
                    duration = video.duration;
                    if(duration && !isNaN(duration)){
                        seekBar.max = duration;
                        seekBar.disabled = false;
                    } else {
                        seekBar.disabled = true;
                    }
                }

                function onSeek(){
                    if(!blockSeek){
                        blockSeek = true;
                        videoWasPaused = video.paused;
                        video.pause();
                    }
                    video.currentTime = seekBar.value;
                }

                function onSeekRelease(){
                    if(!videoWasPaused){
                        video.play();
                    }
                    blockSeek = false;
                }

                function onTimeupdate(){
                    if(!blockSeek){
                        seekBar.value = video.currentTime;
                    }
                }

                //or durationchange
                video.addEventListener('loadedmetadata', enableDisableSeekBar, false);
                video.addEventListener('emptied', enableDisableSeekBar, false);
                video.addEventListener('timeupdate', onTimeupdate, false);

                seekBar.addEventListener('input', onSeek, false);
                seekBar.addEventListener('change', onSeekRelease, false);

                enableDisableSeekBar();
                onTimeupdate();
            }

    Array.prototype.forEach.call(document.querySelectorAll('.mediaplayer'), createPlayer);
})();
