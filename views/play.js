function playAudio() {
	if (music.paused) {
		music.play();
		document.getElementById('i').className = "";
		document.getElementById('i').className = "glyphicon glyphicon-pause playme";
    document.getElementById('playingImage').style.display = 'block';
	} else {
		music.pause();
		document.getElementById('i').className = "";
		document.getElementById('i').className = "glyphicon glyphicon-play playme";
    document.getElementById('playingImage').style.display = 'none';
	}
}
